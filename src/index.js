export const myAlphaNumberT = (nbr) => `${nbr}`;
export const sum = (a, b) => {
  if (typeof a !== 'number' || typeof b !== 'number') {
    return 0;
  }

  return a + b;
};

export const mySizeAlphaT = (str = '') => {
  let count = 0;

  if (typeof str !== 'string') {
    return count;
  }

  while (str[count]) {
    count += 1;
  }

  return count;
};

export const myDisplayAlphaT = () => 'abcdefghijklmnopqrstuvwxyz';

export const myArrayAlphaT = (str) => {
  const result = [];

  for (let i = 0; i < mySizeAlphaT(str); i += 1) {
    result[i] = str[i];
  }

  return result;
};

export const myIsPosiNegT = (nbr) => {
  if (nbr <= 0) {
    return 'NEGATIVE';
  }

  return 'POSITIF';
};

export const fibo = (n) => {
  if (n <= 0) {
    return 0;
  }

  if (n === 1 || n === 2) {
    return 1;
  }

  return fibo(n - 1) + fibo(n - 2);
};

export const myDisplayAlphaReverseT = () => {
  const alpha = myDisplayAlphaT();
  let reverseAlpha = '';

  for (let i = mySizeAlphaT(alpha); i > 0; i -= 1) {
    reverseAlpha += alpha[i - 1];
  }

  return reverseAlpha;
};

export const myLengthArrayT = (arr) => {
  let i = 0;

  while (arr[i]) {
    i += 1;
  }

  return i;
};

export const myDisplayUnicodeT = (arr) => {
  const results = [];

  for (let i = 0; i < arr.length; i += 1) {
    const decimal = arr[i];

    if ((decimal >= 65 && decimal <= 99)) {
      results[i] = String.fromCharCode(arr[i]);
    }
    if ((decimal >= 97 && decimal <= 122)) {
      results[i] = String.fromCharCode(arr[i]);
    }
    if ((decimal >= 48 && decimal <= 57)) {
      results[i] = String.fromCharCode(arr[i]);
    }
    if (decimal === 32) {
      results[i] = String.fromCharCode(arr[i]);
    }
  }

  return results.join('');
};
