import {
  myAlphaNumberT, sum, mySizeAlphaT, myDisplayAlphaT, myArrayAlphaT, myIsPosiNegT, fibo,
  myDisplayAlphaReverseT, myLengthArrayT, myDisplayUnicodeT
} from '../src';

describe('Index', () => {
  /* Exo 0 */
  it('should be return 12 in string when I say 12', () => {
    expect(myAlphaNumberT(12)).toBe('12');
  });
  it('should be return toto when a say toto in string', () => {
    expect(myAlphaNumberT(12)).toBe('12');
  });
  it('should be return undefined in string when I say nothing', () => {
    expect(myAlphaNumberT()).toBe('undefined');
  });

  /* Exo 1 */
  it('should be had 7 when I sum 3 and 4 ', () => {
    expect(sum(3, 4)).toBe(7);
  });
  it('should be had 0 when I sum 3 and ""', () => {
    expect(sum(3, '')).toBe(0);
  });
  it('should be had 0 when I sum nothing', () => {
    expect(sum()).toBe(0);
  });

  /* Exo 2 */
  it('should be had the size 4 for the string toto', () => {
    expect(mySizeAlphaT('toto')).toBe(4);
  });
  it('should be had the size 0 when is not a string', () => {
    expect(mySizeAlphaT(20)).toBe(0);
  });
  it('should be had the size 0 when is empty', () => {
    expect(mySizeAlphaT()).toBe(0);
  });

  /* Exo 3 */
  it('should be had string abcdefghijklmnopqrstuvwxyz when I say 20', () => {
    expect(myDisplayAlphaT(20)).toBe('abcdefghijklmnopqrstuvwxyz');
  });
  it('should be had string abcdefghijklmnopqrstuvwxyz when is empty ', () => {
    expect(myDisplayAlphaT()).toBe('abcdefghijklmnopqrstuvwxyz');
  });

  /* Exo 4 */
  it('should be had empty table if I say nothing', () => {
    expect(myArrayAlphaT()).toEqual([]);
  });
  it('should be had string table [t,o,t,o] when I say toto', () => {
    expect(myArrayAlphaT('toto')).toEqual(['t', 'o', 't', 'o']);
  });
  it('should be had empty table if I say [t,o,t,o]', () => {
    expect(myArrayAlphaT(['t', 'o', 't', 'o'])).toEqual([]);
  });
  it('should be had empty table if I say [2,4,5]', () => {
    expect(myArrayAlphaT([2, 4, 5])).toEqual([]);
  });
  it('should be had empty table if I say 24', () => {
    expect(myArrayAlphaT(24)).toEqual([]);
  });

  /* Exo 5 */
  it('should be return POSITIF if I say 20 ', () => {
    expect(myIsPosiNegT(20)).toBe('POSITIF');
  });
  it('should be return NEGATIVE if I say -20', () => {
    expect(myIsPosiNegT(-20)).toBe('NEGATIVE');
  });
  it('should be return POSITIF if I say toto', () => {
    expect(myIsPosiNegT('toto')).toBe('POSITIF');
  });
  it('should be return POSITIF if I say nothing', () => {
    expect(myIsPosiNegT()).toBe('POSITIF');
  });

  /* Exo 6 */
  it('should be return 0 if I say 0', () => {
    expect(fibo(0)).toBe(0);
  });
  it('should be return 1 if I say 2', () => {
    expect(fibo(2)).toBe(1);
  });
  it('should be return 8 if I say 6', () => {
    expect(fibo(6)).toBe(8);
  });
  it('should be return 12586269025 if I say 50', () => {
    expect(fibo(50)).toBe(12586269025);
  });

  /* Exo 7 */
  const result = 'abcdefghijklmnopqrstuvwxyz'.split('').reverse().join('');
  it('should be return the reverse of abcdefghijklmnopqrstuvwxyz when you say nothing', () => {
    expect(myDisplayAlphaReverseT()).toBe(result);
  });
  it('should be return the reverse of abcdefghijklmnopqrstuvwxyz when you say 20', () => {
    expect(myDisplayAlphaReverseT(20)).toBe(result);
  });

  /* Exo 8 */
  it('should be return 0 when the table is empty', () => {
    expect(myLengthArrayT([])).toBe(0);
  });
  it('should be return 0 when the table is [t]', () => {
    expect(myLengthArrayT(['t'])).toBe(1);
  });
  it('should be return 0 when I say 6', () => {
    expect(myLengthArrayT(6)).toBe(0);
  });

  /* Exo 9 */
  it('should be return "toto" when i say [116, 111, 116, 111]', () => {
    expect(myDisplayUnicodeT([116, 111, 116, 111])).toBe('toto');
  });
  it('should be return " " when i say [32]', () => {
    expect(myDisplayUnicodeT([32])).toBe(' ');
  });
  it('should be return F2 string when i say [1, 70, 50, 20]', () => {
    expect(myDisplayUnicodeT([1, 70, 50, 20])).toBe('F2');
  });
  it('should be return empty string when i say empty input', () => {
    expect(myDisplayUnicodeT([])).toBe('');
  });
  it('should be return empty string when i say 2', () => {
    expect(myDisplayUnicodeT(2)).toBe('');
  });
  it('should be return empty string when i say [p, i, z, z, a]', () => {
    expect(myDisplayUnicodeT(['p', 'i', 'z', 'z', 'a'])).toBe('');
  });
});
